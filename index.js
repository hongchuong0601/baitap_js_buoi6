// bài 1 tìm số nguyên dương nhỏ nhất
var tong = 0
var i=0
var chuong = 0
while(tong <= 10000){
    i++
    tong+=i
}
document.getElementById("result1").innerText=` ${i}`
//bài 2 tính tổng

function tinhTongB2(){
    var soX = document.getElementById("nhap-so-x").value*1;
    var soN = document.getElementById("nhap-so-n").value*1;
    var tinhTong = 1
    
    for ( var i=1; i<= soN; i++){
        tinhTong *= soX
    }
  
    document.getElementById("result2").innerText=` ${tinhTong}`
} 
//bai 3 tính giai thừa
// document.getElementById('tinhGiaiThua').onclick = function (){}

function tinhGiaiThua(){
    var nhapSoB3=document.getElementById("nhap-soB3").value*1
    var giaiThua = 1

    for(var i=1; i<= nhapSoB3; i++){
        giaiThua *= i
    }
    document.getElementById("result3").innerText=` ${giaiThua}`
}

// bài 4 tính chẵn lẽ
// var content = ''
// var itemHTML = ""

function tinhChanLe(){
    var nhapSoB4 = document.getElementById("nhap-soB4").value*1
    var itemHTML = ""
    for (var i=1; i<= nhapSoB4; i++){
        if(i%2 == 0){
            var content = ` <p style="background-color: red; color: white;"> Số chẵn: ${i} </p>`
        } else {
            var content = ` <p style="background-color: blue; color: white;"> Số lẽ: ${i} </p>`
        }
        itemHTML += content
    }

    document.getElementById("result4").innerHTML=` ${itemHTML}`
}